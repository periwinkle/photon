use fltk::prelude::*;
use fltk::{app, button, dialog, frame::Frame, window};
use lazy_static::lazy_static;
use std::fs::{copy, read_dir, create_dir_all};
use std::path::PathBuf;
use std::sync::mpsc::{channel, Sender};
use std::sync::Mutex;
use std::thread;
use std::time::{Duration, SystemTime};
use chrono::DateTime;
use chrono::offset::Local;

lazy_static! {
    static ref SRC: Mutex<PathBuf> = Mutex::new(PathBuf::new());
    static ref DST: Mutex<PathBuf> = Mutex::new(PathBuf::new());
}

static mut READY: (bool, bool) = (false, false);

#[derive(Clone)]
enum MessageType {
    ScanDir,
    Scanned { paths: Vec<FileInfo> },
    InProgress { done: usize },
    Done,
}

#[derive(Clone)]
struct FileInfo {
    pub path: PathBuf,
    pub meta: SystemTime,
}


fn scan_dir(tx: Sender<MessageType>) {
    let to_copy = recurse_dir(SRC.lock().unwrap().clone());
    tx.send(MessageType::Scanned { paths: to_copy }).unwrap();
}

fn recurse_dir(dir: PathBuf) -> Vec<FileInfo> {
    let entries = read_dir(dir).unwrap();
    let mut ret = vec![];
    for i in entries.flatten() {
        let p = i.path();
        if let Ok(m) = i.metadata() {
            if let Ok(t) = m.created() {
                if !m.is_dir() {
                    ret.push(FileInfo { path: p, meta: t });
                } else {
                    ret.append(&mut recurse_dir(p));
                }
            }
        }
    }
    ret
}

fn copy_files(tx: Sender<MessageType>, files: Vec<FileInfo>) {
    let mut base = DST.lock().unwrap().clone();
    let mut existing: Vec<String> = vec![];
    for (idx, file) in files.iter().enumerate() {
        if let Some(p) = file.path.file_name() {
            let mtime: DateTime<Local> = file.meta.into();
            let mtime = format!("{}", mtime.format("%Y-%m-%d"));
            base.push(mtime.clone());
            if !existing.iter().any(|i| i == &mtime) {
                create_dir_all(base.clone()).unwrap();
                existing.push(mtime);
            }
            base.push(p);
            copy(file.path.clone(), base.clone()).unwrap();
            base.pop();
            base.pop();
        }
        if (idx + 1) % 7 == 0 {
            tx.send(MessageType::InProgress { done: idx + 1 }).unwrap();
        }
    }
    tx.send(MessageType::Done).unwrap();
}

fn main() {
    let app = app::App::default();
    let mut win = window::Window::default()
        .with_size(430, 150)
        .center_screen()
        .with_label("Photon");
    win.make_resizable(false);
    let (tx, rx) = channel::<MessageType>();
    let mut go_button = button::Button::new(10, 70, 105, 35, "Go");
    let tx2 = tx.clone();
    go_button.deactivate();
    go_button.set_callback(move |_| tx2.send(MessageType::ScanDir).unwrap());
    let mut src_button = button::Button::new(10, 10, 105, 35, "Choose Source");
    let mut src_select = dialog::FileDialog::new(dialog::FileDialogType::BrowseDir);
    let mut dst_button = button::Button::new(140, 10, 135, 35, "Choose Destination");
    let mut dst_select = dialog::FileDialog::new(dialog::FileDialogType::BrowseDir);
    let mut frm = Frame::new(140, 40, 180, 100, "");
    let mut go1 = go_button.clone();
    src_button.set_callback(move |_| {
        src_select.show();
        *SRC.lock().unwrap() = src_select.filename();
        unsafe {
            READY.0 = true;
            if READY.1 {
                go1.activate();
            }
        }
    });
    let mut go1 = go_button.clone();
    dst_button.set_callback(move |_| {
        dst_select.show();
        *DST.lock().unwrap() = dst_select.filename();
        unsafe {
            READY.1 = true;
            if READY.0 {
                go1.activate();
            }
        }
    });
    win.show();
    let mut files = 0;
    while app.wait() {
        if let Ok(msg) = rx.try_recv() {
            match msg {
                MessageType::ScanDir => {
                    let tx2 = tx.clone();
                    thread::spawn(move || scan_dir(tx2));
                }
                MessageType::Scanned { paths } => {
                    let tx2 = tx.clone();
                    files = paths.len();
                    frm.set_label(&format!("{} files found, starting...", files));
                    frm.redraw_label();
                    thread::sleep(Duration::from_millis(200));
                    thread::spawn(move || copy_files(tx2, paths));
                }
                MessageType::InProgress { done } => {
                    frm.set_label(&format!("{}/{} copied", done, files));
                    frm.redraw_label();
                }
                MessageType::Done => {
                    frm.set_label("Done!");
                    frm.redraw_label();
                    go_button.deactivate();
                }
            }
        }
    }
}
